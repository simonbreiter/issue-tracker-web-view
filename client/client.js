// Libs
import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, browserHistory } from 'react-router'

// Components
import App from './views/App'
import Login from './views/Login'
import IssueTracker from './components/IssueTracker'

ReactDOM.render(
    <Router history={browserHistory}>
        <Route component={App}>
            <Route path="/" component={Login} name="Simon" onSubmit={App.onSubmit} />
            <Route path="/issue-tracker" component={IssueTracker} />
        </Route>
    </Router>,
    document.getElementById('container')
)