import React from 'react';

import { attemptLogin } from '../utils/auth';

const Login = React.createClass({
    getInitialState() {
        return {
            name: "",
            password: "",
            token: ""
        };
    },
    onNameChange(e) {
        this.setState({name: e.target.value})
    },
    onPasswordChange(e) {
        this.setState({password: e.target.value})
    },
    onSubmit(e) {
        e.preventDefault()
        attemptLogin(this.state.name, this.state.password)
    },
    render() {
        return (
            <div>
                <h1>Login</h1>
                <form>
                    <input type="text" value={this.state.name} onChange={this.onNameChange}/>
                    <input type="password" value={this.state.password} onChange={this.onPasswordChange}/>
                    <input type="submit" value="Login" onClick={this.onSubmit}/>
                </form>
            </div>
        )
    }
})

export default Login
