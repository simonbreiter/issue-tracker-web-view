import React from 'react'

const App = React.createClass({
    render() {
        return (
            <div>
                <h1>issue-tracker</h1>
                {this.props.children}
            </div>
        )
    }
})

export default App
