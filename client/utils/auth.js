import { browserHistory } from 'react-router'

function attemptLogin(name, password) {
    fetch('http://localhost:3000/api/authenticate',{
        method: 'POST',
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }),

        body: JSON.stringify({name: name, password: password})

    }).then(function(response) {
        // Convert to JSON
        return response.json()
    }).then(function(jsonResponse) {
        if(jsonResponse.success) {
            localStorage.setItem('token', jsonResponse.token)
            browserHistory.push('/issue-tracker')
        } else {
            // If no user exists, create one
            register(name, password)
        }
    })

}

function register(name, password) {
    fetch('http://localhost:3000/api/users',{
        method: 'POST',
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }),

        body: JSON.stringify({name: name, password: password})

    }).then(function(response) {
        // Convert to JSON
        return response.json()
    }).then(function(jsonResponse) {
        console.log(jsonResponse)
        browserHistory.push('/issue-tracker')
    })
}

export { attemptLogin }
