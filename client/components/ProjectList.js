import React, { Component } from 'react';
import Project from './Project';

const AddProjectForm = React.createClass({
    getInitialState: function () {
        return {text: ""}
    },
    onAddProjectChange: function (e) {
        this.setState({text: e.target.value})
    },
    onAddProject: function (e) {
        e.preventDefault()
        if(this.state.text) {
            this.props.onAddProject(this.state.text)
            this.setState({text: ""})
        }
    },
    render: function () {
        return (
            <div className="add">
                <form action="">
                    <input
                        value={this.state.text}
                        onChange={this.onAddProjectChange}
                        type="text"
                        placeholder="New Project..."/>
                    <input
                        onClick={this.onAddProject}
                        className="btn"
                        type="submit"
                        value="+"/>
                </form>
            </div>
        )
    }
})

const ProjectList = React.createClass({
    onClick: function (text) {
        this.props.onClick(text)
    },
    onAddProject: function (text) {
        this.props.onAddProject(text)
    },
    onSwitchProjects: function(projectOneIndex, projectTwoIndex) {
        this.props.onSwitchProjects(projectOneIndex, projectTwoIndex)
    },
    onChangeProject: function (originalProject, newProject) {
        this.props.onChangeProject(originalProject, newProject)
    },
    render: function () {
        let projects = this.props.projects
        return (
            <div className="project-list">
                <AddProjectForm onAddProject={this.onAddProject}/>
                <ul>
                    {projects.map((project, index) => {
                        return (
                            <Project
                                text={project}
                                key={index}
                                index={index}
                                onClick={this.onClick}
                                onSwitch={this.onSwitchProjects}
                                onChange={this.onChangeProject}
                            />
                        )
                    })}
                </ul>
            </div>
        )
    }
})

export default ProjectList
