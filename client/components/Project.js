import React, { Component, PropTypes } from 'react';
import { findDOMNode } from 'react-dom';
import ItemTypes from './ItemTypes';
import { DragSource, DropTarget } from 'react-dnd';
import flow from 'lodash/flow';

const projectSource = {
    beginDrag(props) {
        return {
            text: props.text,
            index: props.index
        };
    }
};

const projectTarget = {
    hover(props, monitor, component) {
        const dragIndex = monitor.getItem().index
        const hoverIndex = props.index
        if (dragIndex === hoverIndex) {
            return
        }
        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect()
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
        const clientOffset = monitor.getClientOffset()
        const hoverClientY = clientOffset.y - hoverBoundingRect.top

        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            return
        }
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            return
        }

        props.onSwitch(dragIndex, hoverIndex)
        monitor.getItem().index = hoverIndex
    }
};

const Project = React.createClass({
    getInitialState: function () {
        return {text: this.props.text}
    },
    onClick: function (text) {
        this.props.onClick(this.state.text)
    },
    onChange: function (e) {
        e.preventDefault()
        this.props.onChange(this.state.text, e.target.value)
        this.state.text = e.target.value
    },
    render: function () {
        return (
            <li><input type="text" style={{border: 'none'}} onClick={this.onClick} onChange={this.onChange} defaultValue={this.state.text} /></li>
        )
    }
})


export default flow(
    DropTarget(ItemTypes.PROJECT, projectTarget, connect => ({
        connectDropTarget: connect.dropTarget()
    })),
    DragSource(ItemTypes.PROJECT, projectSource, (connect, monitor) => ({
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }))
)(Project);