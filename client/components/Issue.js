import React, { Component, PropTypes } from 'react';
import { findDOMNode } from 'react-dom';
import ItemTypes from './ItemTypes';
import { DragSource, DropTarget } from 'react-dnd';
import flow from 'lodash/flow';

const issueSource = {
    beginDrag(props) {
        return {
            id: props.text,
            index: props.index
        };
    }
};

const issueTarget = {
    hover(props, monitor, component) {
        const dragIndex = monitor.getItem().index
        const hoverIndex = props.index
        if (dragIndex === hoverIndex) {
            return
        }
        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect()
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
        const clientOffset = monitor.getClientOffset()
        const hoverClientY = clientOffset.y - hoverBoundingRect.top

        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            return
        }
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            return
        }

        props.onSwitch(dragIndex, hoverIndex)
        monitor.getItem().index = hoverIndex
    }
};

const Issue = React.createClass({
    getInitialState: function() {
        return {
            text: this.props.text
        }
    },

    onClick: function() {
        this.props.onClick(this.state.text)
    },

    onSwitch: function(indexOne, indexTwo) {
        this.props.onSwitch(indexOne, indexTwo)
    },

    onSuccess: function() {
        this.props.onSuccess()
    },

    onError: function(text) {
        this.props.onError(text)
    },

    onChange: function(e) {
        if(e.key === 'Enter') {
            if (e.target.value.match(/^.*\s{1}[0-9]{1,2}\.{1}[0-9]{1,2}\.{1}[0-9]{4}\s{1}[a-zA-Z0-9]+$/)) {
                this.props.onChange(this.state.text, e.target.value)
                this.state.text = e.target.value
                this.onSuccess()
            } else {
                this.onError(e.target.value)
                e.target.value = this.state.text
            }
        }
    },

    render: function() {
        const { text, isDragging, connectDragSource, connectDropTarget } = this.props;
        const opacity = isDragging ? 0 : 1;

        return connectDragSource(connectDropTarget(
            <li style={this.props.toggled ? {textDecoration: 'line-through', color: 'grey', opacity: opacity} : { opacity: opacity }}>
                <input checked={this.props.toggled ? 'checked' : ''} type="checkbox" onClick={this.onClick} />
                <input type="text" style={{border: 'none'}} onKeyPress={this.onChange} defaultValue={this.state.text} />
            </li>
        ));
    }
})


export default flow(
    DropTarget(ItemTypes.ISSUE, issueTarget, connect => ({
        connectDropTarget: connect.dropTarget()
    })),
    DragSource(ItemTypes.ISSUE, issueSource, (connect, monitor) => ({
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }))
)(Issue);