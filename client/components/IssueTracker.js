import React from "react"
import IssueList from "./IssueList"
import ProjectList from "./ProjectList"
import { browserHistory } from 'react-router'
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

const IssueTracker = React.createClass({
    getInitialState: function() {
        let projects = JSON.parse(localStorage.getItem('projects')) || {
                "node-tracker": {
                    uuid: "123-123-123",
                    issues: {
                        "foo 10.10.2019 simon": true,
                        "bar 10.10.2019 simon": false,
                    }
                },
                "awesome-app": {
                    uuid: "123-123-123",
                    issues: {
                        "foo 1.10.2019 emanuele": true,
                        "bar 10.12.2019 emanuele": false,
                        "baz 10.10.2019 emanuele": false
                    }
                }
            }

        return ({
            activeProject: localStorage.getItem('activeProject') || 'node-tracker',
            projects: projects,
            token: localStorage.getItem('token'),
            errorMessage: ""
        })
    },
    componentDidMount: function() {
        if(!this.state.token) {
            browserHistory.push("/")
        }
    },
    onAddProject: function (text) {
        this.state.projects[text] = {issues: {}}
        localStorage.setItem('projects', JSON.stringify(this.state.projects))
        this.setState(this.state)
    },
    onSwitchProjects: function (projectOneIndex, projectTwoIndex) {
        let projectOne = Object.keys(this.state.projects)[projectOneIndex]
        let projectTwo = Object.keys(this.state.projects)[projectTwoIndex]

        this.onChangeProject(projectOne, "aFullyRandomSequence123")
        this.onChangeProject(projectTwo, projectOne)
        this.onChangeProject("aFullyRandomSequence123", projectTwo)

        localStorage.setItem('projects', JSON.stringify(this.state.projects))
        this.setState(this.state)
    },
    onChangeProject: function (originalProject, newProject) {
        let projects = JSON.stringify(this.state.projects)
        projects = projects.replace('"'+originalProject+'":', '"'+newProject+'":')
        this.state.projects = JSON.parse(projects)
        this.state.activeProject = newProject
        localStorage.setItem('projects', JSON.stringify(this.state.projects))
        localStorage.setItem('activeProject', this.state.activeProject)
        this.setState(this.state)
    },
    onAddIssue: function (text) {
        this.state.projects[this.state.activeProject].issues[text] = false
        localStorage.setItem('projects', JSON.stringify(this.state.projects))
        this.setState(this.state)
    },
    onToggleIssue: function (issue) {
        let issueState = !this.state.projects[this.state.activeProject].issues[issue]
        this.state.projects[this.state.activeProject].issues[issue] = issueState
        localStorage.setItem('projects', JSON.stringify(this.state.projects))
        this.setState(this.state)
    },
    onSwitchIssues: function (issueOneIndex, issueTwoIndex) {
        let issueOne = Object.keys(this.state.projects[this.state.activeProject].issues)[issueOneIndex]
        let issueOneState = this.state.projects[this.state.activeProject].issues[issueOne]

        let issueTwo = Object.keys(this.state.projects[this.state.activeProject].issues)[issueTwoIndex]
        let issueTwoState = this.state.projects[this.state.activeProject].issues[issueTwo]

        this.onChangeIssue(issueOne, "aFullyRandomSequence123")
        this.onChangeIssue(issueTwo, issueOne)
        this.onChangeIssue("aFullyRandomSequence123", issueTwo)

        console.log("Replace [" + issueOne + " | "+issueOneState+"] with ["+issueTwo+" | "+issueTwoState+"]")

        this.state.projects[this.state.activeProject].issues[issueOne] = issueOneState
        this.state.projects[this.state.activeProject].issues[issueTwo] = issueTwoState

        localStorage.setItem('projects', JSON.stringify(this.state.projects))
        this.setState(this.state)
    },
    onChangeIssue: function (originalIssue, newIssue) {
        let issues = JSON.stringify(this.state.projects[this.state.activeProject].issues)
        issues = issues.replace('"'+originalIssue+'":', '"'+newIssue+'":')
        this.state.projects[this.state.activeProject].issues = JSON.parse(issues)
        localStorage.setItem('projects', JSON.stringify(this.state.projects))
        this.setState(this.state)
    },
    onRemoveIssues: function() {
        let issues = this.state.projects[this.state.activeProject].issues
        for (let issue in issues) {
            if(issues[issue]) {
                delete issues[issue]
            }
        }
        localStorage.setItem('projects', JSON.stringify(this.state.projects))
        this.setState(this.state)
    },
    onErrorMessageIssue: function(text) {
        this.state.errorMessage = "Invalid format '" + text + "' should be like 'my issue 10.10.2017 asignee'"
        this.setState(this.state)
    },
    onSuccessMessageIssue: function() {
        this.state.errorMessage = ""
        this.setState(this.state)
    },
    loadIssues: function(project) {
        this.state.activeProject = project
        localStorage.setItem('activeProject', this.state.activeProject)
        this.setState(this.state)
    },
    render: function () {
        return (
            <div id="app">
                <ProjectList
                    projects={Object.keys(this.state.projects)}
                    onAddProject={this.onAddProject}
                    onSwitchProjects={this.onSwitchProjects}
                    onChangeProject={this.onChangeProject}
                    onClick={this.loadIssues}
                />
                <IssueList
                    activeProject={this.state.activeProject}
                    issues={this.state.projects[this.state.activeProject].issues}
                    errorMessage={this.state.errorMessage}
                    onAddIssue={this.onAddIssue}
                    onToggleIssue={this.onToggleIssue}
                    onSwitchIssues={this.onSwitchIssues}
                    onChangeIssue={this.onChangeIssue}
                    onRemoveIssues={this.onRemoveIssues}
                    onErrorMessageIssue={this.onErrorMessageIssue}
                    onSuccessMessageIssue={this.onSuccessMessageIssue}
                />
            </div>
        )
    }
})

export default DragDropContext(HTML5Backend)(IssueTracker)
