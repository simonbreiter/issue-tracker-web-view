import React, { Component } from 'react';
import Issue from './Issue';

const AddIssueForm = React.createClass({
    getInitialState: function () {
        return {text: ""}
    },
    onAddIssueChange: function (e) {
        this.setState({text: e.target.value})
    },
    onSuccessMessageIssue: function() {
        this.props.onSuccessMessageIssue()
    },
    onErrorMessageIssue: function(text) {
        this.props.onErrorMessageIssue(text)
    },
    onAddIssue: function (e) {
        e.preventDefault()
        if(this.state.text && this.state.text.match(/^.*\s{1}[0-9]{1,2}\.{1}[0-9]{1,2}\.{1}[0-9]{4}\s{1}[a-zA-Z0-9]+$/)) {
            this.props.onAddIssue(this.state.text)
            this.setState({text: ""})
            this.onSuccessMessageIssue()
        } else {
            this.onErrorMessageIssue(this.state.text)
        }
    },
    render: function () {
        return (
            <div className="add">
                <form action="">
                    <input
                        value={this.state.text}
                        onChange={this.onAddIssueChange}
                        type="text"
                        placeholder="New issue..."/>
                    <input
                        onClick={this.onAddIssue}
                        className="btn"
                        type="submit"
                        value="+" />
                </form>
            </div>
        )
    }
})

const IssueList = React.createClass({
    getInitialState: function() {
        return {
            errorMessage: ""
        }
    },
    onErrorMessageIssue: function(text) {
        this.props.onErrorMessageIssue(text)
    },
    onSuccessMessageIssue: function() {
        this.props.onSuccessMessageIssue()
    },
    onAddIssue: function (text) {
        this.props.onAddIssue(text)
    },
    onToggleIssue: function(toggle) {
        this.props.onToggleIssue(toggle)
    },
    onSwitchIssues: function(issueOneIndex, issueTwoIndex) {
        this.props.onSwitchIssues(issueOneIndex, issueTwoIndex)
    },
    onChangeIssue: function(originalIssue, newIssue) {
        this.props.onChangeIssue(originalIssue, newIssue)
    },
    onRemoveIssues: function() {
        this.props.onRemoveIssues()
    },
    render: function () {
        let issues = this.props.issues
        return (
            <div className="issue-list">
                <div>{this.props.activeProject}</div>
                <AddIssueForm
                    onAddIssue={this.onAddIssue}
                    onErrorMessageIssue={this.onErrorMessageIssue}
                    onSuccessMessageIssue={this.onSuccessMessageIssue}
                />
                <div className="issues">
                    <ul>
                        {Object.keys(issues).map((issue, index) => {
                            return (
                                <Issue
                                    text={issue}
                                    key={issue}
                                    index={index}
                                    toggled={issues[issue]}
                                    onClick={this.onToggleIssue}
                                    onSwitch={this.onSwitchIssues}
                                    onChange={this.onChangeIssue}
                                    onError={this.onErrorMessageIssue}
                                    onSuccess={this.onSuccessMessageIssue}
                                />
                            )
                        })}
                    </ul>
                    <p><span style={{'color': 'red'}}>{this.props.errorMessage}</span></p>
                    <button onClick={this.onRemoveIssues}>Remove closed issues</button>
                </div>
            </div>
        )
    }
})

export default IssueList

