let path = require('path');

module.exports = {
    context: path.join(__dirname, 'client'),
    entry: {
        javascript: './client.js',
        html: './index.html'
    },
    output: {
        path: path.join(__dirname, 'client/public'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            { exclude: /(node_modules|bower_components)/, test: /\.js$/, loader: 'babel' },
            { test: /\.html$/, loader: 'file?name=[name].[ext]' }
        ]
    },
    devServer: {
        historyApiFallback: true
    }
};
