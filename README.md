# Issue Tracker Web View [![Build status](https://gitlab.com/simonbreiter/issue-tracker-web-view/badges/master/build.svg)](https://gitlab.com/simonbreiter/issue-tracker-web-view) [![License](http://img.shields.io/:license-mit-blue.svg)](http://doge.mit-license.org) [![Developed By](https://img.shields.io/badge/developed%20with%20♥%20by-Emanuele%20&%20Simon-red.svg)](https://emanuelemazzotta.com/)

A simple issue tracker web view developed in ReactJS.

## Development

### Install Dependencies

Install dependencies first with

```sh
yarn install
```

### Client Development

Build your client with

```sh
yarn client:build
```

You can now start the dev environment with

```sh
yarn client:dev
```

Webpack-dev-server is now running on http://localhost:8080/webpack-dev-server/

### Server Development

To start server development

```sh
yarn run server:dev
```

A server is now running on http://localhost:3000


## Deployment

Build docker

```sh
yarn docker:build
```

and start with

```sh
yarn docker:start
```

## Authors

[Emanuele Mazzotta](mailto:hello@mazzotta.me)

[Simon Breiter](mailto:hello@simonbreiter.com)

## License

[MIT License](LICENSE.md) © Emanuele Mazzotta, Simon Breiter
