FROM node:8.12.0-alpine

LABEL maintainer="hello@mazzotta.me"

RUN mkdir /app
WORKDIR /app
ADD . /app

RUN yarn install
RUN yarn build

CMD ["yarn", "dev"]
