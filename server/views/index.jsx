import React from 'react';

class IssueTrackerMain extends React.Component {
    render() {
        return (
            <html>
            <head>
                <title>issue-tracker-web-view</title>
            </head>
            <body id="app">

            <div id="container">Loading...</div>
            <script src="/public/js/bundle.js"></script>

            </body>
            </html>
        );
    }
}

module.exports = IssueTrackerMain;
