'use strict';
let express = require('express');
let app = express();

app.set('view engine', 'jsx');
app.engine('jsx', require('express-react-views').createEngine());

app.use('/client/public', express.static(__dirname + '/public'));

app.get('/', require('./routes').index);
app.get('/v1/api/:name', function(req, res) {
    res.status(200).json({'hello': req.params.name})
});

app.listen(3000, function () {
    console.log('App listening on port 3000!');
});
